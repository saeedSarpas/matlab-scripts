% Resolution
N = 100;

% Domain
x = linspace(0,1,N+1);
dx = x(2:end) - x(1:end-1);

% IC: Sinusoid
u_int = -1/(2*pi)*cos(2*pi*x);
u = (u_int(2:end) - u_int(1:end-1)) ./ dx;

% IC: Step func
% u = ones(N,1);
% u(1:N/2) = -1;

% IC: over/under density
% u = zeros(N, 1);
% u(N/2-4:N/2) = -1;
% u(N/2:N/2+3) = 1;

% Time step
dt = 0.01;
for t = 0:dt:3
    % ODE Solver
    [t, u] = ode45(@burger, [0,dt], u);
    u = u(end,:);
    
    plot([x(1:end-1); x(2:end)], [u; u], 'linewidth', 7);
    ylim([-1.1,1.1]);
    
    drawnow;
    pause(dt);
end
