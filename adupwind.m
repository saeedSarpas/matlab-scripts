function [dudt] = adupwind(t, u)
    N = length(u);
    
    x = linspace(0,1,N+2);
    dx = x(3:end) - x(2:end-1);

    uL = [0; u(1:end-2); 0];   
    uC = [0; u(2:end-1); 0];
    uR = [0; u(3:end); 0];
    
    a = -1.0;

    % \partial_t u = - U * \partial_x u + \kappa \partial^2_x u
    dudt = (-a ./ dx) .* ((uC - uL) .* (a > 0) + (uR - uC) .* (a <= 0));
    dudt = dudt(:,end);
end