function [dudt] = burger(t, u)
    N = length(u);
    
    x = linspace(0,1,N+1);
    dx = x(2:end) - x(1:end-1);
    
    f = u.^2 / 2;
    dfdu = u
    
    uL = u(1:end-1);
    uR = u(2:end);
    maxF = max(f(1:end-1), f(2:end));
    minF = min(f(1:end-1), f(2:end));
    minF(logical((uR > 0) .* (0 > uL))) = 0;
    f_int = maxF .* (uL > uR) + minF .* (uL <= uR);
    f_int = [0; f_int; 0];

    dudt = (f_int(1:end-1) - f_int(2:end)) ./ dx';
end