function [dudt] = cdsolver(t, u)
    N = length(u);
    
    x = linspace(0,1,N+2);
    dx = x(3:end) - x(2:end-1);

    uL = [0; u(1:end-2); 0];   
    uC = [0; u(2:end-1); 0];
    uR = [0; u(3:end); 0];
    
    a = 1.0;
    kappa = 0.05;

    % \partial_t u = - a * \partial_x u + \kappa \partial^2_x u
    dudt = -a .* (uR - uL) ./ (2 * dx) + kappa * (uL - 2 * uC + uR) ./ dx.^2;
    dudt = dudt(:,end);
end
